package com.changchihchieh.canvas;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.PictureDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

/** 簡易小畫布
 * 
 * @author chih-chieh chang
 *
 */
@SuppressLint({ "ClickableViewAccessibility", "DrawAllocation" })
public class CanvasView extends ViewGroup {
	/** 繪圖模式 */
	public enum PaintMode {
		/** 筆 */
		PEN,
		/** 直線 */
		STRAIGHTLINE,
		/** 曲線 */
		CURVELINE,
		/** 實心多邊形 */
		FILLPOLYGON,
		/** 空心多邊形 */
		STROKEPOLYGON,
		/** 實心矩形 */
		FILLRECT,
		/** 空心矩形 */
		STROKERECT,
		/** 實心橢圖 */
		FILLELLIPSE,
		/** 空心橢圖 */
		STROKEELLIPSE,
		/** 橡皮擦 */
		ERASER
	}
	
	public interface OnCanvasPaintingListener {
		/** Motion Down時呼叫 */
		public void onPaintingBegin();
		/** Motion Move時呼叫 */
		public void onPainting();
		/** Motion Up時呼叫 */
		public void onPaintingEnd();
	}
	
	private class CanvasPathObject implements Cloneable {
		/** 多點觸碰時用來辨識touch的變數，值是touchDownTime+touchIndex(理論上保證不會重覆) */
		public long identifier;
		public PaintMode pathType;
		public ArrayList<PointF> points = new ArrayList<PointF>();
		public int strokeColor;
		public int fillColor;
		public float lineWidth;
		public Boolean isFinished;
		
		public CanvasPathObject(long identifier, PaintMode pathType) {
			super();
			this.identifier = identifier;
			this.pathType = pathType;
			this.isFinished = false;
			this.points.clear();
			
		}
		
		@Override
		protected void finalize() throws Throwable {
			
			points.clear();
			points = null;
			
			super.finalize();
		}
		
		@Override
		protected CanvasPathObject clone() throws CloneNotSupportedException {
			CanvasPathObject cloneObject = new CanvasPathObject(this.identifier, this.pathType);
			cloneObject.points.addAll(this.points);
			cloneObject.strokeColor = this.strokeColor;
			cloneObject.fillColor = this.fillColor;
			cloneObject.lineWidth = this.lineWidth;
			cloneObject.isFinished = this.isFinished;
			
			return cloneObject;
		}
		
		@Override
		public int hashCode() {
			return super.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			return super.equals(o);
		}
		
	}
	
	private static final Boolean useTextureView = false;
	
	private Boolean drawable = true;
	private PaintMode paintingMode = PaintMode.PEN;
	private OnCanvasPaintingListener paintingListener;
	private int strokeColor = Color.BLACK;
	private int fillColor = Color.BLACK;
	private float lineWidth = 5;
	
	private Boolean multipleTouchesEnabled = true;
	private Boolean isMultipleTouchesMode = multipleTouchesEnabled; // for private usage
	
	private ArrayList<CanvasPathObject> paths = new ArrayList<CanvasView.CanvasPathObject>();
	private ArrayList<CanvasPathObject> undoPaths = new ArrayList<CanvasView.CanvasPathObject>();
	private int currentCurveIndex;
	
	private SurfaceView surfaceView;
	private TextureView textureView;
	private Picture pt;
	
	public CanvasView(Context context) {
		this(context, null, 0);
	}
	
	public CanvasView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
	public CanvasView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		if (useTextureView) {
			if (!isInEditMode()) {
				((Activity) context).getWindow().setFlags(
					    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
					    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
				((Activity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
				setLayerType(View.LAYER_TYPE_HARDWARE, null);
			}
			
			textureView = new TextureView(context);
			textureView.setBackgroundColor(Color.TRANSPARENT);
			addView(textureView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
			textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
				
				@Override
				public void onSurfaceTextureUpdated(SurfaceTexture surface) {
				}
				
				@Override
				public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
				}
				
				@Override
				public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
					return false;
				}
				
				@Override
				public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
					Canvas cv = textureView.lockCanvas();
					cv.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
					textureView.unlockCanvasAndPost(cv);
				}
			});
		}
		else {
			surfaceView = new SurfaceView(context);
			addView(surfaceView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
			if (!isInEditMode()) {
				surfaceView.setZOrderOnTop(true);
			}
			surfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
			surfaceView.setBackgroundColor(Color.TRANSPARENT);
		}
		
		paths.clear();
		undoPaths.clear();
	}
	
	@Override
	protected void finalize() throws Throwable {
		
		paintingListener = null;
		
		textureView = null;
//		surfaceView = null;
		pt = null;
		paths.clear();
		paths = null;
		undoPaths.clear();
		undoPaths = null;
		
		super.finalize();
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		
		if (changed && getChildCount() > 0) {
			View child = getChildAt(0);
			child.layout(0, 0, (r-l), (b-t));
		}
	}
	
	@Override
	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
		
		if (!useTextureView) {
			if (pt != null) {
				canvas.saveLayerAlpha(0, 0, canvas.getWidth(), canvas.getHeight(), 255, Canvas.ALL_SAVE_FLAG);
				canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
				PictureDrawable pd = new PictureDrawable(pt);
				pd.setBounds(0, 0, getWidth(), getHeight());
				pd.draw(canvas);
				canvas.restore();
			}
		}
		
		
		return super.drawChild(canvas, child, drawingTime);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if (useTextureView) {
			return;
		}
		
		if (pt == null || textureView == null) {
			return;
		}
		
		Canvas cv = textureView.lockCanvas();
		cv.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		pt.draw(cv);
		textureView.unlockCanvasAndPost(cv);
		
		PictureDrawable pd = new PictureDrawable(pt);
		pd.setBounds(0, 0, getWidth(), getHeight());
		pd.draw(canvas);
		
	}
	
	@Override
	public void draw(Canvas canvas) {
		if (useTextureView) {
			if (pt != null) {
				canvas.saveLayerAlpha(0, 0, canvas.getWidth(), canvas.getHeight(), 255, Canvas.ALL_SAVE_FLAG);
				canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
				PictureDrawable pd = new PictureDrawable(pt);
				pd.setBounds(0, 0, getWidth(), getHeight());
				pd.draw(canvas);
				canvas.restore();
			}
		}
		else {
			super.draw(canvas);
		}
	}
	
	private void draw() {
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		
		int width = getWidth();
		int height = getHeight();
		if (width + height == 0) {
			measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
			layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
			width = getWidth();
			height = getHeight();
		}
		
		pt = new Picture();
		Canvas paintingCanvas = pt.beginRecording(width, height);
		
//		paint.reset();
//		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//		paintingCanvas.drawPaint(paint);
		
		synchronized (paths) {
			for (CanvasPathObject pathObject : paths) {
				paint.reset();
				paint.setAntiAlias(true);
				drawPath(paintingCanvas, paint, pathObject);
			}
		}
		
		pt.endRecording();
		
//		invalidate();
		
		if (useTextureView) {
			if (textureView.isAvailable()) {
				Canvas cv = textureView.lockCanvas();
				cv.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
				pt.draw(cv);
				textureView.unlockCanvasAndPost(cv);
			}
		}
		else {
			Canvas cv = surfaceView.getHolder().lockCanvas();
			cv.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
			pt.draw(cv);
			surfaceView.getHolder().unlockCanvasAndPost(cv);
		}
	}
	
	private void drawPath(Canvas canvas, Paint paint, CanvasPathObject pathObject) {
		if (pathObject.points.size() == 0) {
			return;
		}
		
		switch (pathObject.pathType) {
		case PEN:
		case STRAIGHTLINE: {
			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(pathObject.strokeColor);
			paint.setStrokeWidth(pathObject.lineWidth);
			
			canvas.drawPoint(pathObject.points.get(0).x, pathObject.points.get(0).y, paint);
			if (pathObject.points.size() > 1) {
				Path path = new Path();
				path.moveTo(pathObject.points.get(0).x, pathObject.points.get(0).y);
				for (int j = 1; j < pathObject.points.size(); j ++) {
					path.lineTo(pathObject.points.get(j).x, pathObject.points.get(j).y);
				}
				
				canvas.drawPath(path, paint);
			}
			
			break;
		}
		case CURVELINE: {
			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(pathObject.strokeColor);
			paint.setStrokeWidth(pathObject.lineWidth);
			
			canvas.drawPoint(pathObject.points.get(0).x, pathObject.points.get(0).y, paint);
			if (pathObject.points.size() == 2) {
				canvas.drawLine(pathObject.points.get(0).x, pathObject.points.get(0).y, pathObject.points.get(1).x, pathObject.points.get(1).y, paint);
			}
			else if (pathObject.points.size() == 3) {
				Path path = new Path();
				path.moveTo(pathObject.points.get(0).x, pathObject.points.get(0).y);
				path.quadTo(pathObject.points.get(1).x, pathObject.points.get(1).y, pathObject.points.get(2).x, pathObject.points.get(2).y);
				
				canvas.drawPath(path, paint);
			} 
			else if (pathObject.points.size() == 4) {
				Path path = new Path();
				path.moveTo(pathObject.points.get(0).x, pathObject.points.get(0).y);
				path.cubicTo(pathObject.points.get(1).x, pathObject.points.get(1).y, pathObject.points.get(2).x, pathObject.points.get(2).y, pathObject.points.get(3).x, pathObject.points.get(3).y);
				
				canvas.drawPath(path, paint);
			}
			
			break;
		}
		case FILLPOLYGON: {
			PointF pFirst = pathObject.points.get(0);
			PointF pLast = pathObject.points.get(pathObject.points.size()-1);
			if (pFirst.equals(pLast.x, pLast.y) && pathObject.points.size() > 1) {
				paint.setStyle(Paint.Style.FILL);
				paint.setColor(pathObject.fillColor);
				paint.setStrokeCap(Paint.Cap.ROUND);
				paint.setStrokeJoin(Paint.Join.ROUND);
				
				Path path = new Path();
				path.moveTo(pathObject.points.get(0).x, pathObject.points.get(0).y);
				for (int j = 1; j < pathObject.points.size(); j ++) {
					path.lineTo(pathObject.points.get(j).x, pathObject.points.get(j).y);
				}
				
				path.close();
				
				canvas.drawPath(path, paint);
				
				paint.reset();
				paint.setAntiAlias(true);
			}
			
		}
		case STROKEPOLYGON: {
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(pathObject.strokeColor);
			paint.setStrokeWidth(pathObject.lineWidth);
			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStrokeJoin(Paint.Join.ROUND);
			
			canvas.drawPoint(pathObject.points.get(0).x, pathObject.points.get(0).y, paint);
			if (pathObject.points.size() > 1) {
				Path path = new Path();
				path.moveTo(pathObject.points.get(0).x, pathObject.points.get(0).y);
				for (int j = 1; j < pathObject.points.size(); j ++) {
					path.lineTo(pathObject.points.get(j).x, pathObject.points.get(j).y);
				}
				
				canvas.drawPath(path, paint);
			}
			
			break;
		}
		case FILLRECT: {
			if (pathObject.points.size() == 2) {
				float minX = Math.min(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float minY = Math.min(pathObject.points.get(0).y, pathObject.points.get(1).y);
				float maxX = Math.max(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float maxY = Math.max(pathObject.points.get(0).y, pathObject.points.get(1).y);
				
				paint.setStyle(Paint.Style.FILL);
				paint.setColor(pathObject.fillColor);
				
				canvas.drawRect(minX, minY, maxX, maxY, paint);
				
				paint.reset();
				paint.setAntiAlias(true);
			}
			
		}
		case STROKERECT: {
			canvas.drawPoint(pathObject.points.get(0).x, pathObject.points.get(0).y, paint);
			if (pathObject.points.size() == 2) {
				float minX = Math.min(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float minY = Math.min(pathObject.points.get(0).y, pathObject.points.get(1).y);
				float maxX = Math.max(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float maxY = Math.max(pathObject.points.get(0).y, pathObject.points.get(1).y);
				
				paint.setStyle(Paint.Style.STROKE);
				paint.setColor(pathObject.strokeColor);
				paint.setStrokeWidth(pathObject.lineWidth);
				
				canvas.drawRect(minX, minY, maxX, maxY, paint);
			}
			
			break;
		}
		case FILLELLIPSE: {
			if (pathObject.points.size() == 2) {
				float minX = Math.min(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float minY = Math.min(pathObject.points.get(0).y, pathObject.points.get(1).y);
				float maxX = Math.max(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float maxY = Math.max(pathObject.points.get(0).y, pathObject.points.get(1).y);
				
				paint.setStyle(Paint.Style.FILL);
				paint.setColor(pathObject.fillColor);
				
				canvas.drawOval(new RectF(minX, minY, maxX, maxY), paint);
				
				paint.reset();
				paint.setAntiAlias(true);
			}
			
		}
		case STROKEELLIPSE: {
			canvas.drawPoint(pathObject.points.get(0).x, pathObject.points.get(0).y, paint);
			if (pathObject.points.size() == 2) {
				float minX = Math.min(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float minY = Math.min(pathObject.points.get(0).y, pathObject.points.get(1).y);
				float maxX = Math.max(pathObject.points.get(0).x, pathObject.points.get(1).x);
				float maxY = Math.max(pathObject.points.get(0).y, pathObject.points.get(1).y);
				
				paint.setStyle(Paint.Style.STROKE);
				paint.setColor(pathObject.strokeColor);
				paint.setStrokeWidth(pathObject.lineWidth);
				
				canvas.drawOval(new RectF(minX, minY, maxX, maxY), paint);
			}
			
			break;
		}
		case ERASER: {
			paint.setAlpha(getAlpha(100));
			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStyle(Paint.Style.STROKE);
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
			paint.setStrokeWidth(pathObject.lineWidth*2);
			
			canvas.drawPoint(pathObject.points.get(0).x, pathObject.points.get(0).y, paint);
			if (pathObject.points.size() > 1) {
				Path path = new Path();
				path.moveTo(pathObject.points.get(0).x, pathObject.points.get(0).y);
				for (int j = 1; j < pathObject.points.size(); j ++) {
					path.lineTo(pathObject.points.get(j).x, pathObject.points.get(j).y);
				}
				
				canvas.drawPath(path, paint);
			}
			
			break;
		}
		default:
			break;
		}
	}
	
	private int getAlpha(int a) { 
		return (int)(255*(a/100f));
	}
	
	// -- Public Functions
	
	/**
	* <p>用來設定是否可以隱藏(因為ViewGroup無效，才用此方法)</p>
	* @author Robert Chou didi31139@gmail.com
	* @date 2016年1月8日 下午3:19:02
	* @version 
	*/
	public void setVisible(Boolean isvisible){				
		if (isvisible) {
			if (useTextureView) {
				textureView.setVisibility(View.VISIBLE);
			}
			else {
				surfaceView.setVisibility(View.VISIBLE);
			}
			
			try {
				draw();	
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}else{
			if (useTextureView) {
				textureView.setVisibility(View.GONE);
			}
			else {
				surfaceView.setVisibility(View.GONE);
			}
		}
		
		this.setVisibility(isvisible? View.VISIBLE: View.GONE);
	}
	
	/** 開閉繪圖功能
	 * 
	 * @author chih-chieh chang
	 * 
	 * @param drawable true or false
	 * 
	 */
	public void setDrawable(Boolean drawable) {
		this.drawable = drawable;
	}
	
	/** 取得是否可繪圖
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return true or false
	 * 
	 */
	public Boolean isDrawable() {
		return drawable;
	}
	
	/** 設定繪圖模式
	 * 
	 * @author chih-chieh chang
	 * 
	 * @param mode 繪圖模式
	 * 
	 */
	public void setPaintingMode(PaintMode mode) {
		this.paintingMode = mode;
		
		if (paths.size() > 0) {
			synchronized (this.paths) {
				for (CanvasPathObject pathObject : paths) {
					if (!pathObject.isFinished) {
						pathObject.isFinished = true;
					}
				}
			}
		}
		
		if (undoPaths.size() > 0) {
			for (int i = 0; i < undoPaths.size(); i ++) {
				CanvasPathObject pathObject = undoPaths.get(i);
				if (!pathObject.isFinished) {
					undoPaths.remove(i);
					-- i;
				}
			}
		}
		
		if (mode == PaintMode.CURVELINE || 
			mode == PaintMode.STROKEPOLYGON || 
			mode == PaintMode.FILLPOLYGON) {
			
			isMultipleTouchesMode = false;
		}
		else {
			isMultipleTouchesMode = this.multipleTouchesEnabled;
		}
	}
	
	/** 取得當前繪圖模式
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return 繪圖模式
	 * 
	 */
	public PaintMode getPaintingMode() {
		return paintingMode;
	}
	
	/** 設定callback
	 * 
	 * @author chih-chieh chang
	 * 
	 * @param listener callback
	 * 
	 */
	public void setOnCanvasPaintingListener(OnCanvasPaintingListener listener) {
		this.paintingListener = listener;
	}
	
	/** 取得當前callback
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return 當前callback
	 * 
	 */
	public OnCanvasPaintingListener getOnCanvasPaintingListener() {
		return paintingListener;
	}
	
	/** 設定線條顏色
	 * 
	 * @author chih-chieh chang
	 * 
	 * @param strokeColor 線條顏色
	 * 
	 */
	public void setStrokeColor(int strokeColor) {
		this.strokeColor = strokeColor;
	}

	/** 取得當前線條顏色
	 * 
	 * @author chih-chieh chang
	 *  
	 * @return 當前線條顏色
	 * 
	 */
	public int getStrokeColor() {
		return strokeColor;
	}
	
	/** 設定填滿顏色
	 * 
	 * @author chih-chieh chang
	 * 
	 * @param strokeColor 填滿顏色
	 * 
	 */
	public void setFillColor(int fillColor) {
		this.fillColor = fillColor;
	}
	
	/** 取得當前填滿顏色
	 * 
	 * @author chih-chieh chang
	 *  
	 * @return 當前填滿顏色
	 * 
	 */
	public int getFillColor() {
		return fillColor;
	}
	
	/** 設定線條粗細
	 * 
	 * @author chih-chieh chang
	 * 
	 * @param lineWidth 線條粗細
	 * 
	 */
	public void setLineWidth(float lineWidth) {
		this.lineWidth = lineWidth;
	}
	
	/** 取得當前線條粗細
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return 當前線條粗細
	 */
	public float getLineWidth() {
		return lineWidth;
	}
	
	/** 取得手繪筆數
	 * 
	 * @return 手繪筆數
	 */
	public int getNumberOfPaints() {
		return this.paths.size();
	}
	
	/** 取得是否可以多點繪畫<br>
	 * 注意:{@linkplain CanvasView#getPaintingMode()}在{@linkplain CanvasView.PaintMode#CURVELINE}, {@linkplain CanvasView.PaintMode#FILLPOLYGON}或{@linkplain CanvasView.PaintMode#STROKEPOLYGON}時
	 * 僅支援單點繪畫。
	 * 
	 * @return true or false
	 * 
	 * @see CanvasView#getPaintingMode()
	 * @see CanvasView.PaintMode
	 */
	public Boolean isMultipleTouchesEnabled() {
		return isMultipleTouchesMode;
	}

	/** 設定是否可以多點繪畫
	 * 
	 * @param multipleTouchesEnabled true or false
	 */
	public void setMultipleTouchesEnabled(Boolean multipleTouchesEnabled) {
		this.multipleTouchesEnabled = multipleTouchesEnabled;
		
		synchronized (this.paths) {
			for (CanvasPathObject pathObject : paths) {
				if (!pathObject.isFinished) {
					pathObject.isFinished = true;
				}
			}
		}
		
		if (paintingMode != PaintMode.CURVELINE && 
			paintingMode != PaintMode.STROKEPOLYGON && 
			paintingMode != PaintMode.FILLPOLYGON) {
				
			isMultipleTouchesMode = multipleTouchesEnabled;
		}
	}
	
	/** 是否可以還原上一步
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return true or false
	 * 
	 */
	public Boolean canUndo() {
		if (!drawable) return false;
		return (paths.size() > 0);
	}
	
	/** 還原上一步
	 * 
	 * @author chih-chieh chang
	 * 
	 */
	public void undo() {
		if (!canUndo()) {
			return;
		}
		
		CanvasPathObject pathObject = paths.get(paths.size()-1);
		if (pathObject.pathType == PaintMode.CURVELINE) {
			if (!pathObject.isFinished && pathObject.points.size() > 2) {
				try {
					CanvasPathObject copyPathObject = pathObject.clone();
					undoPaths.add(copyPathObject);
					if (pathObject.points.size() == 3) {
						pathObject.points.remove(1);
					}
					else {
						pathObject.points.remove(currentCurveIndex);
					}
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
					undoPaths.add(pathObject);
					paths.remove(paths.size()-1);
				}
			}
			else {
//				pathObject.isFinished = true;
				undoPaths.add(pathObject);
				paths.remove(paths.size()-1);
			}
		}
		else if (pathObject.pathType == PaintMode.FILLPOLYGON || pathObject.pathType == PaintMode.STROKEPOLYGON) {
			if (!pathObject.isFinished && pathObject.points.size() > 2) {
				try {
					CanvasPathObject copyPathObject = pathObject.clone();
					undoPaths.add(copyPathObject);
					pathObject.points.remove(pathObject.points.size()-1);
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
					undoPaths.add(pathObject);
					paths.remove(paths.size()-1);
				}
			}
			else {
//				pathObject.isFinished = true;
				undoPaths.add(pathObject);
				paths.remove(paths.size()-1);
			}
		}
		else {
			pathObject.isFinished = true;
			undoPaths.add(pathObject);
			paths.remove(paths.size()-1);
		}
		
		draw();
	}
	
	/** 是否可以取消還原
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return true or false
	 * 
	 */
	public Boolean canRedo() {
		if (!drawable) {
			return false;
		}
		return (undoPaths.size() > 0);
	}
	
	/** 取消還原
	 * 
	 * @author chih-chieh chang
	 * 
	 */
	public void redo() {
		if (!canRedo()) {
			return;
		}
		
		CanvasPathObject pathObject = undoPaths.get(undoPaths.size()-1);
		if (paths.size() > 0 && !pathObject.isFinished) {
			CanvasPathObject lastPathObject = paths.get(paths.size()-1);
			if (!lastPathObject.isFinished) {
				paths.set(paths.size()-1, pathObject);
			}
			else {
				paths.add(pathObject);
			}
		}
		else {
			paths.add(pathObject);
		}
		undoPaths.remove(undoPaths.size()-1);
		
		draw();
	}
	
	/** 清空畫布
	 * 
	 * @author chih-chieh chang
	 * 
	 */
	public void clear() {
		if (!drawable) {
			return;
		}
		
		paths.clear();
		undoPaths.clear();
		
		draw();
	}
	
	/** 取得繪制的圖片
	 * 
	 * @author chih-chieh chang
	 * 
	 * @return 圖片
	 * 
	 */
	public Bitmap getOutputImage() {
		Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas newCanvas = new Canvas(bitmap);
		this.draw(newCanvas);
		
		if (useTextureView) {
			if (pt != null && textureView != null) {
				pt.draw(newCanvas);
			}
		}
		else {
			if (pt != null && surfaceView != null) {
				pt.draw(newCanvas);
			}
		}
		
		return bitmap;
	}
	
	// -- Touch Events
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!drawable) {
			return false;
		}
		
		if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
			onTouchEventBegin(event);
		}
		else if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
			// 有一个非主要的手指按下了.
			if (isMultipleTouchesMode) {
				onTouchEventBegin(event);
			}
			else {
				return false;
			}
		}
		else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			onTouchEventMoving(event);
		}
		else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
			onTouchEventEnd(event);
		}
		else if (event.getActionMasked() == MotionEvent.ACTION_CANCEL) {
			onTouchEventEnd(event);
		}
		else if (event.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
			// 一个非主要的手指抬起来了
			if (isMultipleTouchesMode) {
				onTouchEventEnd(event);
			}
			else {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public boolean onTrackballEvent(MotionEvent event) {
		return onTouchEvent(event);
	}
	
	private void onTouchEventBegin(MotionEvent event) {
		int actionIndex = event.getActionIndex();
		int pointerId = event.getPointerId(actionIndex);
		long pointID = event.getDownTime()+pointerId;
		PointF point = new PointF(event.getX(actionIndex), event.getY(actionIndex));
		
		undoPaths.clear();
		
		if (paths.size() == 0) {
			CanvasPathObject newPathObject = new CanvasPathObject(pointID, paintingMode);
			newPathObject.points.add(point);
			newPathObject.strokeColor = strokeColor;
			newPathObject.fillColor = fillColor;
			newPathObject.lineWidth = lineWidth;
			paths.add(newPathObject);
			
			currentCurveIndex = 1;
		}
		else {
			CanvasPathObject pathObject = null;
			for (CanvasPathObject p : paths) {
				if (p.identifier == pointID) {
					pathObject = p;
					break;
				}
			}
			if (!isMultipleTouchesMode) {
				pathObject = paths.get(paths.size()-1);
			}
			
			if (pathObject != null) {
				if (isMultipleTouchesMode) {
					pathObject.isFinished = true;
				}
				if (!pathObject.isFinished) {
					if (pathObject.pathType == PaintMode.CURVELINE) {
						if (pathObject.points.size() == 1) {
							pathObject.identifier = pointID;
							
							currentCurveIndex = 1;
							pathObject.points.add(point);
						}
						else if (pathObject.points.size() == 2) {
							pathObject.identifier = pointID;
							
							currentCurveIndex = 1;
							pathObject.points.add(1, point);
						}
						else if (pathObject.points.size() == 3) {
							PointF point1 = pathObject.points.get(0);
							PointF point2 = pathObject.points.get(1);
							PointF point3 = pathObject.points.get(2);
							
							pathObject.identifier = pointID;
							
							if (distanceBetweenPoints(point1, point2) < distanceBetweenPoints(point2, point3)
									&& distanceBetweenPoints(point1, point) > distanceBetweenPoints(point, point3)) {
								currentCurveIndex = 2;
								pathObject.points.add(2, point);
							}
							else {
								currentCurveIndex = 1;
								pathObject.points.add(1, point);
							}
						}
						else if (pathObject.points.size() == 4) {
							pathObject.isFinished = true;
						}
					}
					else if (pathObject.pathType == PaintMode.FILLPOLYGON || 
							pathObject.pathType == PaintMode.STROKEPOLYGON) {
						if (pathObject.points.size() > 1) {
							PointF pFirst = pathObject.points.get(0);
							PointF pLast = pathObject.points.get(pathObject.points.size()-1);
							if (pFirst.equals(pLast.x, pLast.y)) {
								pathObject.isFinished = true;
							}
							else {
								pathObject.identifier = pointID;
								
								pathObject.points.add(point);
							}
						}
						else {
							pathObject.identifier = pointID;
							
							pathObject.points.add(point);
						}
					}
					else {
						pathObject.isFinished = true;
					}
				}
				
				if (pathObject.isFinished) {
					CanvasPathObject newPathObject = new CanvasPathObject(pointID, paintingMode);
					newPathObject.points.add(point);
					newPathObject.strokeColor = strokeColor;
					newPathObject.fillColor = fillColor;
					newPathObject.lineWidth = lineWidth;
					paths.add(newPathObject);
					
					currentCurveIndex = 1;
				}
			}
			else {
				CanvasPathObject newPathObject = new CanvasPathObject(pointID, paintingMode);
				newPathObject.points.add(point);
				newPathObject.strokeColor = strokeColor;
				newPathObject.fillColor = fillColor;
				newPathObject.lineWidth = lineWidth;
				paths.add(newPathObject);
				
				currentCurveIndex = 1;
			}
			
		}
		
		draw();
		
		if (paintingListener != null) {
			paintingListener.onPaintingBegin();
		}
	}		
	
	private void onTouchEventMoving(MotionEvent event) {
		int pointCount = event.getPointerCount();
		for (int i = 0; i < pointCount; i ++) {
			if (!isMultipleTouchesMode && i > 0) {
				break;
			}
			
			int pointerId = event.getPointerId(i);
			long pointID = event.getDownTime()+pointerId;
			PointF point = new PointF(event.getX(i), event.getY(i));
			CanvasPathObject pathObject = null;
			for (CanvasPathObject p : paths) {
				if (p.identifier == pointID) {
					pathObject = p;
					break;
				}
			}
			
			if (pathObject != null) {
				switch (pathObject.pathType) {
				case PEN:
				case ERASER: {
					pathObject.points.add(point);
					break;
				}
				case STRAIGHTLINE:
				case FILLRECT:
				case STROKERECT:
				case FILLELLIPSE:
				case STROKEELLIPSE: {
					if (pathObject.points.size() == 1) {
						pathObject.points.add(point);
					}
					else {
						pathObject.points.set(1, point);
					}
					break;
				}
				case CURVELINE: {
					if (pathObject.points.size() == 1) {
						currentCurveIndex = 1;
						pathObject.points.add(point);
					}
					else {
						pathObject.points.set(currentCurveIndex, point);
					}
					break;
				}
				case FILLPOLYGON:
				case STROKEPOLYGON: {
					if (pathObject.points.size() == 1) {
						pathObject.points.add(point);
					}
					else {
						pathObject.points.set(pathObject.points.size()-1, point);
					}
					break;
				}
				default:
					break;
				}
			}
			else {
				CanvasPathObject newPathObject = new CanvasPathObject(pointID, paintingMode);
				newPathObject.points.add(point);
				newPathObject.strokeColor = strokeColor;
				newPathObject.fillColor = fillColor;
				newPathObject.lineWidth = lineWidth;
				paths.add(newPathObject);
			}
		}
		
		draw();
		
		if (paintingListener != null) {
			paintingListener.onPainting();
		}
	}
	
	private void onTouchEventEnd(MotionEvent event) {
		int actionIndex = event.getActionIndex();
		int pointerId = event.getPointerId(actionIndex);
		long pointID = event.getDownTime()+pointerId;
		PointF point;
		if (actionIndex < event.getPointerCount()) {
			point = new PointF(event.getX(actionIndex), event.getY(actionIndex));
		}
		else {
			point = new PointF(event.getX(), event.getY());
		}
		
		CanvasPathObject pathObject = null;
		for (CanvasPathObject p : paths) {
			if (p.identifier == pointID) {
				pathObject = p;
				break;
			}
		}
		
		if (pathObject != null) {
			if (isMultipleTouchesMode) {
				pathObject.isFinished = true;
			}
			else {
				if (pathObject.pathType == PaintMode.CURVELINE) {
				}
				else if (pathObject.pathType == PaintMode.FILLPOLYGON || pathObject.pathType == PaintMode.STROKEPOLYGON) {
					PointF pFirst = pathObject.points.get(0);
					if (distanceBetweenPoints(point, pFirst) < 20.0 && pathObject.points.size() > 2) {
						pathObject.points.set(pathObject.points.size()-1, pFirst);
						pathObject.isFinished = true;
					}
				}
				else {
					pathObject.isFinished = true;
				}
			}
		}
		
		draw();
		
		if (paintingListener != null) {
			paintingListener.onPaintingEnd();
		}
	}
	
	// -- Others
	
	private double distanceBetweenPoints(PointF point1, PointF point2) {
		return Math.sqrt(Math.pow((point1.x-point2.x), 2)+Math.pow((point1.y-point2.y), 2));
	}
	
}
